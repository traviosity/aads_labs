#include "stdafx.h"
#include "CppUnitTest.h"
#include "../lab_two/Map.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace MapUnitTests
{
	TEST_CLASS(MapUnitTests)
	{
	public:
		Map<int, char>* testMap;

		// before each test
		TEST_METHOD_INITIALIZE(setUp)
		{
			testMap = new Map<int, char>();
		}
		// after each test
		TEST_METHOD_CLEANUP(cleanUp)
		{
			delete testMap;
		}

		TEST_METHOD(InsertWithNotValidInput)
		{
			try
			{
				testMap->Insert(0, '0');
				testMap->Insert(1, '0');
				testMap->Insert(0, '0');
			}
			catch (exception e)
			{
				Assert::AreEqual("inserting data already exist in tree", e.what());
			}
		}

		TEST_METHOD(InsertWithValidInput)
		{
			testMap->Insert(0, '0');
			testMap->Insert(1, '1');
			testMap->Insert(2, '2');
			Assert::AreEqual(*testMap->GetValues()->at(0), '0');
			Assert::AreEqual(*testMap->GetValues()->at(1), '1');
			Assert::AreEqual(*testMap->GetValues()->at(2), '2');
		}

		TEST_METHOD(RemoveWithValidInput)
		{
			testMap->Insert(0, '0');
			testMap->Insert(1, '1');
			testMap->Insert(2, '2');
			testMap->Insert(3, '3');
			Assert::IsTrue(testMap->Remove(1));
			Assert::AreEqual(*testMap->GetValues()->at(0), '0');
			Assert::AreEqual(*testMap->GetValues()->at(1), '2');
			Assert::AreEqual(*testMap->GetValues()->at(2), '3');
		}

		TEST_METHOD(RemoveWithNotValidInput)
		{
			testMap->Insert(0, '0');
			testMap->Insert(1, '1');
			testMap->Insert(2, '2');
			Assert::IsFalse(testMap->Remove(100));
			Assert::AreEqual(*testMap->GetValues()->at(0), '0');
			Assert::AreEqual(*testMap->GetValues()->at(1), '1');
			Assert::AreEqual(*testMap->GetValues()->at(2), '2');
		}
	};
}