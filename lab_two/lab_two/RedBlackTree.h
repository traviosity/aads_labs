#pragma once

#include "stdafx.h"
#include "SinglyLinkedList.h"

template <class T> class RedBlackTree
{
protected:
	enum Color { black, red };

	struct Node
	{
		Node* parent;
		Node* left;
		Node* right;
		Color color;

		T *data;

		Node()
		{}
	};

	Node* root;
	Node* NIL;

	void rotateLeft(Node *const n);
	void rotateRight(Node *const n);

	void GetAllRecordsIter(Node* node, SinglyLinkedList<T>* result)
	{
		if (node == NIL)
			return;

		GetAllRecordsIter(node->left, result);
		result->push_back(node->data);
		GetAllRecordsIter(node->right, result);
	}

	void ClearIter(Node* node)
	{
		if (node == NIL)
			return;

		ClearIter(node->left);
		ClearIter(node->right);
		delete node;
	}

	void AfterInsertValidCheck(Node * n);
	void AfterRemoveValidCheck(Node * n);

	Node* FindNode(T const data)
	{
		Node *current = root;
		while (current != NIL)
			if (data == *current->data)
				return current;
			else
				current = data < *current->data ? current->left : current->right;

		return nullptr;
	}

public:
	unsigned int length;

	RedBlackTree();
	~RedBlackTree();

	void Insert(T* const data);
	T* Remove(T* const data);
	SinglyLinkedList<T>* GetAllRecords();
	void Clear();

	friend ostream & operator << (ostream &out, RedBlackTree<T> &tree)
	{
		out << *tree.GetAllRecords();
		return out;
	}
};

template<class T>
RedBlackTree<T>::RedBlackTree()
{
	NIL = new Node();
	NIL->parent = nullptr;
	NIL->left = NIL;
	NIL->right = NIL;
	NIL->color = black;

	length = 0;
	root = NIL;
}

template<class T>
RedBlackTree<T>::~RedBlackTree()
{
	Clear();
}

template<class T>
void RedBlackTree<T>::rotateRight(Node *const node)
{
	if (node == nullptr)
		throw invalid_argument("null reference exception in rotate right");

	Node* pivot = node->left;

	node->left = pivot->right;
	if (pivot->right != NIL)
		pivot->right->parent = node;

	if (pivot != NIL)
		pivot->parent = node->parent;
	if (node->parent != nullptr)
	{
		if (node->parent->left == node)
			node->parent->left = pivot;
		else
			node->parent->right = pivot;
	}
	else
	{
		root = pivot;
		root->color = black;
	}

	pivot->right = node;
	if (node != NIL)
		node->parent = pivot;
}

template<class T>
void RedBlackTree<T>::rotateLeft(Node *const node)
{
	if (node == nullptr)
		throw invalid_argument("null reference exception in rotate left");

	Node * pivot = node->right;

	node->right = pivot->left;
	if (pivot->left != NIL)
		pivot->left->parent = node;

	if (pivot != NIL)
		pivot->parent = node->parent;
	if (node->parent != nullptr)
	{
		if (node->parent->left == node)
			node->parent->left = pivot;
		else
			node->parent->right = pivot;
	}
	else
	{
		root = pivot;
		root->color = black;
	}

	pivot->left = node;
	if (node != NIL)
		node->parent = pivot;
}

template<class T>
void RedBlackTree<T>::Insert(T* const data)
{
	Node *current, *parent, *newNode;

	/* find where node belongs */
	current = root;
	parent = nullptr;
	while (current != NIL)
	{
		if (*data == *current->data)
			throw invalid_argument("inserting data already exist in tree");

		parent = current;
		current = (*data < *current->data) ?
			current->left : current->right;
	}

	/* setup new node */
	newNode = new Node();
	newNode->parent = parent;
	newNode->left = NIL;
	newNode->right = NIL;
	newNode->color = red;
	newNode->data = data;

	/* insert node in tree */
	if (parent)
	{
		if (*data < *parent->data)
			parent->left = newNode;
		else
			parent->right = newNode;
	}
	else
		root = newNode;

	AfterInsertValidCheck(newNode);

	length++;
}

template<class T>
void RedBlackTree<T>::AfterInsertValidCheck(Node * newNode)
{
	/* check Red-Black properties */
	while (newNode != root && newNode->parent->color == red)
	{
		if (newNode->parent == newNode->parent->parent->left)
		{
			Node *uncle = newNode->parent->parent->right;
			if (uncle->color == red)
			{
				/* uncle is RED */
				newNode->parent->color = black;
				uncle->color = black;
				newNode->parent->parent->color = red;
				newNode = newNode->parent->parent;
			}
			else
			{
				/* uncle is BLACK */
				if (newNode == newNode->parent->right)
				{
					/* make newNode a left child */
					newNode = newNode->parent;
					rotateLeft(newNode);
				}

				/* recolor and rotate */
				newNode->parent->color = red;
				newNode->parent->parent->color = red;
				rotateRight(newNode->parent->parent);
			}
		}
		else
		{
			/* mirror image of above code */
			Node *uncle = newNode->parent->parent->left;
			if (uncle->color == red)
			{

				/* uncle is RED */
				newNode->parent->color = black;
				uncle->color = black;
				newNode->parent->parent->color = red;
				newNode = newNode->parent->parent;
			}
			else
			{
				/* uncle is BLACK */
				if (newNode == newNode->parent->left)
				{
					newNode = newNode->parent;
					rotateRight(newNode);
				}

				/* recolor and rotate */
				newNode->parent->color = black;
				newNode->parent->parent->color = red;
				rotateLeft(newNode->parent->parent);
			}
		}
	}

	root->color = black;
}

template<class T>
T* RedBlackTree<T>::Remove(T* const data)
{
	Node* nodeToDelete = FindNode(*data);

	if (nodeToDelete == nullptr)
		return nullptr;

	T* dataToReturn = nodeToDelete->data;

	Node* tempNodeToDelete;
	if (nodeToDelete->left == NIL || nodeToDelete->right == NIL)
	{
		/* y has a NIL node as a child */
		tempNodeToDelete = nodeToDelete;
	}
	else
	{
		/* find tree successor with a NIL node as a child */
		tempNodeToDelete = nodeToDelete->right;
		while (tempNodeToDelete->left != NIL)
			tempNodeToDelete = tempNodeToDelete->left;
	}

	Node* tempNodeChild;
	if (tempNodeToDelete->left != NIL)
		tempNodeChild = tempNodeToDelete->left;
	else
		tempNodeChild = tempNodeToDelete->right;

	/* remove y from the parent chain */
	tempNodeChild->parent = tempNodeToDelete->parent;
	if (tempNodeToDelete->parent)
		if (tempNodeToDelete == tempNodeToDelete->parent->left)
			tempNodeToDelete->parent->left = tempNodeChild;
		else
			tempNodeToDelete->parent->right = tempNodeChild;
	else
		root = tempNodeChild;

	if (tempNodeToDelete != nodeToDelete) nodeToDelete->data = tempNodeToDelete->data;

	if (tempNodeToDelete->color == black)
		AfterRemoveValidCheck(tempNodeChild);

	delete tempNodeToDelete;

	length--;

	return dataToReturn;
}

template<class T>
void RedBlackTree<T>::AfterRemoveValidCheck(Node * nodeToDelete)
{
	while (nodeToDelete != root && nodeToDelete->color == black)
	{
		if (nodeToDelete == nodeToDelete->parent->left)
		{
			Node *brother = nodeToDelete->parent->right;
			if (brother->color == red)
			{
				brother->color = black;
				nodeToDelete->parent->color = red;
				rotateLeft(nodeToDelete->parent);
				brother = nodeToDelete->parent->right;
			}

			if (brother->left->color == black && brother->right->color == black)
			{
				brother->color = red;
				nodeToDelete = nodeToDelete->parent;
			}
			else
			{
				if (brother->right->color == black)
				{
					brother->left->color = black;
					brother->color = red;
					rotateRight(brother);
					brother = nodeToDelete->parent->right;
				}

				brother->color = nodeToDelete->parent->color;
				nodeToDelete->parent->color = black;
				brother->right->color = black;
				rotateLeft(nodeToDelete->parent);
				nodeToDelete = root;
			}
		}
		else
		{
			Node *brother = nodeToDelete->parent->left;
			if (brother->color == red)
			{
				brother->color = black;
				nodeToDelete->parent->color = red;
				rotateRight(nodeToDelete->parent);
				brother = nodeToDelete->parent->left;
			}

			if (brother->right->color == black && brother->left->color == black)
			{
				brother->color = red;
				nodeToDelete = nodeToDelete->parent;
			}
			else
			{
				if (brother->left->color == black)
				{
					brother->right->color = black;
					brother->color = red;
					rotateLeft(brother);
					brother = nodeToDelete->parent->left;
				}

				brother->color = nodeToDelete->parent->color;
				nodeToDelete->parent->color = black;
				brother->left->color = black;
				rotateRight(nodeToDelete->parent);
				nodeToDelete = root;
			}
		}
	}

	nodeToDelete->color = black;
}

template<class T>
SinglyLinkedList<T>* RedBlackTree<T>::GetAllRecords()
{
	SinglyLinkedList<T>* result = new SinglyLinkedList<T>();

	GetAllRecordsIter(root, result);

	return result;
}

template<class T>
void RedBlackTree<T>::Clear()
{
	ClearIter(root);

	root = NIL;
}