#include "pch.h"
#include "HuffmanEncoder.h"

int main()
{
	HuffmanEncoder encoder = HuffmanEncoder("test string for show how huffman coding algorithm works/");
	cout << "initial string (memory capacity - " << encoder.initStr.length() * 8 << " bits): '" << encoder.initStr << "'" << endl;
	cout << "coded string (memory capacity - " << encoder.CodedString().length() << " bits): '" << encoder.CodedString() << endl;
	cout << "encoded string (memory capacity - " << encoder.EncodedString(encoder.CodedString()).length() * 8 << " bits): '" << encoder.EncodedString(encoder.CodedString()) << "'" << endl;
	cout << "compression ratio - " << (float)encoder.CodedString().length() / (float)(encoder.initStr.length() * 8) << endl;
	getchar();

	return 0;
}