#pragma once
#include "Map.h"

struct HuffmanTreeElement
{
	int numOfReiteration;
	SinglyLinkedList<char> charsInChild;

	friend ostream & operator << (ostream &out, HuffmanTreeElement &tree)
	{
		out << "[" << tree.numOfReiteration << ", " << tree.charsInChild << "]";
		return out;
	}

	friend bool operator==(const HuffmanTreeElement& left, const HuffmanTreeElement& right)
	{
		return left.numOfReiteration == right.numOfReiteration && left.charsInChild == right.charsInChild;
	}

	friend bool operator!=(const HuffmanTreeElement& left, const HuffmanTreeElement& right)
	{
		return !(left == right);
	}
};

class HuffmanEncoder
{
	void GenerateCodes();

public:
	Map<char, string> codes;
	string initStr;

	HuffmanEncoder(string str);
	string CodedString();
	string EncodedString(string codedString);
};
