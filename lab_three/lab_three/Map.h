#pragma once

#include "pch.h"
#include "RedBlackTree.h"

template <class KeyType, class ValueType> struct KeyValuePair
{
	KeyType key;
	ValueType value;

	friend bool operator==(const KeyValuePair& left, const KeyValuePair& right)
	{
		return left.key == right.key;
	}

	friend bool operator<(const KeyValuePair& left, const KeyValuePair& right)
	{
		return left.key < right.key;
	}

	KeyValuePair<KeyType, ValueType>()
	{}

	KeyValuePair<KeyType, ValueType>(KeyType key, ValueType value) : key(key), value(value)
	{}

	KeyValuePair<KeyType, ValueType>(KeyType key) : key(key)
	{}

	friend ostream & operator << (ostream &out, KeyValuePair<KeyType, ValueType> &pair)
	{
		out << "[" << pair.key << ", " << pair.value << "]";

		return out;
	}
};

template <class KeyType, class ValueType> class Map : public RedBlackTree<KeyValuePair<KeyType, ValueType>>
{

public:
	Map();
	~Map();

	void Insert(KeyType key, ValueType value);
	ValueType Remove(KeyType key);
	void Clear();
	SinglyLinkedList<KeyType>* GetKeys();
	SinglyLinkedList<ValueType>* GetValues();

	bool Find(KeyType key, ValueType ** out = nullptr)
	{
		if (RedBlackTree<KeyValuePair<KeyType, ValueType>>::FindNode(KeyValuePair<KeyType, ValueType>(key)) != nullptr)
		{
			if (out != nullptr)
				*out = &RedBlackTree<KeyValuePair<KeyType, ValueType>>::FindNode(KeyValuePair<KeyType, ValueType>(key))->data->value;
			return true;
		}

		return false;
	}
};

template<class KeyType, class ValueType>
Map<KeyType, ValueType>::Map() : RedBlackTree<KeyValuePair<KeyType, ValueType>>()
{}

template<class KeyType, class ValueType>
Map<KeyType, ValueType>::~Map()
{}

template<class KeyType, class ValueType>
void Map<KeyType, ValueType>::Insert(KeyType key, ValueType value)
{
	RedBlackTree<KeyValuePair<KeyType, ValueType>>::Insert(new KeyValuePair<KeyType, ValueType>(key, value));
}

template<class KeyType, class ValueType>
ValueType Map<KeyType, ValueType>::Remove(KeyType key)
{
	KeyValuePair<KeyType, ValueType>* result;
	if ((result = RedBlackTree<KeyValuePair<KeyType, ValueType>>::Remove(new KeyValuePair<KeyType, ValueType>(key))) != nullptr)
		return result->value;
}


template<class KeyType, class ValueType>
void Map<KeyType, ValueType>::Clear()
{
	RedBlackTree<KeyValuePair<KeyType, ValueType>>::Clear();
}

template<class KeyType, class ValueType>
SinglyLinkedList<KeyType>* Map<KeyType, ValueType>::GetKeys()
{
	SinglyLinkedList<KeyValuePair<KeyType, ValueType>>* records = RedBlackTree<KeyValuePair<KeyType, ValueType>>::GetAllRecords();

	SinglyLinkedList<KeyType>* result = new SinglyLinkedList<KeyType>();
	while (records->length != 0)
	{
		result->push_back(new KeyType(records->pop_front()->key));
	}

	return result;
}

template<class KeyType, class ValueType>
SinglyLinkedList<ValueType>* Map<KeyType, ValueType>::GetValues()
{
	SinglyLinkedList<KeyValuePair<KeyType, ValueType>>* records = RedBlackTree<KeyValuePair<KeyType, ValueType>>::GetAllRecords();

	SinglyLinkedList<ValueType>* result = new SinglyLinkedList<ValueType>();
	while (records->length != 0)
	{
		result->push_back(new ValueType(records->pop_front()->value));
	}

	return result;
}
