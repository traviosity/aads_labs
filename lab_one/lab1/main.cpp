#include "stdafx.h"
#include "SinglyLinkedList.h"

int main()
{
	SinglyLinkedList<int> list = SinglyLinkedList<int>();

	list.push_back(new int(0));
	list.push_back(new int(1));
	list.push_back(new int(2));
	cout << list;
	getchar();
	return 0;
}